\documentclass[../main.tex]{subfiles}
 
\begin{document}
Im Folgenden wird es nötig sein, die lower Norm eines Operators zu berechnen, um aus dieser auf die Norm der Inversen zu schließen, selbst wenn dieser zwischen unendlich dimensionalen Vektorräumen wirkt. Diese Methode ist nicht für alle Operatoren gleich gut anwendbar, aber in den hier auftretenden Fällen ist sie sehr nützlich, deshalb werden hier lediglich Tridiagonaloperatoren $T$ zwischen $\ell^2(\Z)$ bzw. $\ell^2(\N)$ betrachtet. Mehr zu dem hier besprochenen Verfahren findet sich in \cite{Givens} und \cite{unvoll}.

Die lower Norm wurde ja in \ref{def:lownorm} über
\[\nu(T) = \inf_{x \in X, \norm{x \leq 1}} \norm{Tx}_Y \]
definiert. Um eine solche lower Norm für einen Bandoperator zu berechnen, werden Restriktionen des Operators auf gewisse Untervektorräume betrachtet. Wir definieren hierzu $J_n^k \coloneqq \{k+1,...,k+n\}$ für ein $k \in \Z$. Dadurch ergeben sich etwa für den Fibonacci-Hamilton Operator Restriktionen $A|_{\ell^2(J_n^k)}$, die wie folgt aussehen:
\[
\footnotesize
A=\vcenter{\hbox{\begin{tikzpicture}[every node/.style={anchor=base,text height=0.3cm,text width=0.3cm}]
    \matrix (M) [matrix of nodes,inner sep=3pt,nodes={rectangle, inner sep=1.5pt,outer sep=2pt,text depth=0cm, text height = 10pt, text width = 10pt, anchor=center,left}, left delimiter={[}, right delimiter={]} ]
    {
    \ & \ & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
    \ & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
    0 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
    0 & 0 & 1 & 1 & 1 & 0 & 0 & 0 & 0 & 0\\
    0 & 0 & 0 & 1 & \boxed{0} & 1 & 0 & 0 & 0 & 0\\
    0 & 0 & 0 & 0 & 1 & 1 & 1 & 0 & 0 & 0\\
    0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & 0 & 0\\
    0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 0\\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & \ \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \ & \ \\
    };
    \draw[dotted, line width=1pt] (M-1-1.north west) to (M-2-2.north west);
    \draw[dotted, line width=1pt] (M-2-1.north west) to (M-3-2.north west);
    \draw[dotted, line width=1pt] (M-1-2.north west) to (M-2-3.north west);
    \draw[dotted, line width=1pt] (M-9-8.south east) to (M-10-9.south east);
    \draw[dotted, line width=1pt] (M-9-9.south east) to (M-10-10.south east);
    \draw[dotted, line width=1pt] (M-8-9.south east) to (M-9-10.south east);
    \draw[opacity=0.5, fill=gray, draw=none] (M-1-5.north west) rectangle (M-10-8.south west);
    \end{tikzpicture}}}
\quad
A|_{\ell^2(J_3^{-1})}=\left[
    \begin{array}{ccc}
        \vdots & \vdots & \vdots\\
        0 & 0 & 0\\
        1 & 0 & 0\\
        0 & 1 & 0\\
        1 & 1 & 1\\
        0 & 1 & 0\\
        0 & 0 & 1\\
        0 & 0 & 0\\
        \vdots & \vdots & \vdots\\
    \end{array}
\right]
\normalsize
\]
Für ein gegebenes $n \in \N$ kann man nun mittels dieser Restriktionen die lower Norm von $A$ einschließen, nämlich gilt:
\begin{theorem}[siehe \cite{Coburn} und \cite{unvoll}]
    Es gilt für ein $T :\ell^p(M) \to \ell^p(M)$ wobei $M \in \{\N, \Z\}$, dass
\[\inf_{k \in \Z}\ \nu(T|_{\ell^2(J_k^n)}) - \delta \leq \nu(T) \leq \inf_{k \in \Z}\ \nu(T|_{\ell^2(J_k^n)})\]
wobei für $\delta$ gilt:
\begin{enumerate}
\item[Falls ]$M= \Z$:
\[ \delta \leq 2 \Big( \sup_{j \in \Z} \abs{a_{j+1,j}} + \sup_{j \in \Z} \abs{a_{j-1,j}} \Big) \sin \Big( \frac{\pi}{2n + 2} \Big) \]
\item[Falls ]$M= \N$:
\[ \delta \leq 2 \Big( \sup_{j \in \N} \abs{a_{j+1,j}} + \sup_{j \in \N} \abs{a_{j-1,j}} \Big) \sin \Big( \frac{\pi}{2n} \Big) \]
\end{enumerate}
\end{theorem}
Dieses bedeutet, dass durch die Berechnung der kleinsten lower Normen aller Restriktionen sich die lower Norm von $T$ selbst ergibt. Jedoch bleibt hierbei immer noch ein Problem. Im Allgemeinen könnte es ja auch unendlich viele verschiedene dieser Restriktionen geben, was die Berechnung der lower Norm an einem Computer mittels dieser Formel effektiv unmöglich machen würde.\\
Im Falle des Fibonacci-Hamilton Operators gibt es jedoch nur endlich viele solcher Restriktionen. Dies liegt daran, dass das Alphabet, aus dem das Wort gebaut ist, nur zwei Buchstaben enthält, somit ist die Anzahl der Teilwörter der Länge $n$ durch $2^n$ begrenzt. Zusätzlich ist das Fibonacci-Wort ein Sturmsches Wort, besitzt also nicht $2^n$, sondern nur $n+1$ Teilwörter der Länge $n$.\\
Für das $\delta$ ergibt sich für den Fibonacci-Hamilton Operator $\delta = 4 \cdot \sin(\frac{\pi}{2n+2})$, da die Nebendiagonalen konstant $1$ sind.

Jedoch ist nicht wirklich die lower Norm für uns interessant, sondern vorallem die Norm der Inversen, aber es gilt ja $\norm{A^{-1}} = \frac{1}{\nu(A)}$ und somit folgt, dass $\norm{A}^{-1}$ auch eingeschlossen werden kann:
\[ \frac{1}{\inf_{k \in \Z}\ \nu(T|_{\ell^2(J_k^n)})} \leq \norm{A^{-1}} \leq \frac{1}{\inf_{k \in \Z}\ \nu(T|_{\ell^2(J_k^n)}) - \delta}\]
Jetzt wird diese Methode verwendet, um zunächst die lower Norm und dann die Norm der Inversen des Zweiseitigen und danach des einseitigen Fibonacci-Hamilton Operators zu berechnen. Hierzu benötigen wir eine Methode, um möglichst effizient die Faktoren des Fibonacci-Wortes zu errechnen, diese wurde in \cite{Dennis} erarbeitet. Somit ergibt sich folgender MATLAB code:
\begin{lstlisting}
n=100;
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
sv=zeros(1,n+1);

for l=1:n+1
    A(2:n+1,:)=diag(ones(1,n-1),1)+diag(Fib(l,:))+diag(ones(1,n-1),-1);
    sv(l)=min(svd(A));
end

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));
iv=[1/(val),1/(val-delta)];
\end{lstlisting}
Dieser Code berechnet eine numerische Einschließung von $\norm{A^{-1}}= \frac{1}{\nu(A)}$, über Restriktionen der Länge $n=100$. Durch \texttt{sturm(n)} wird eine Matrix erzeugt, die alle $n+1$ Teilwörter der Länge $n$ enthält; diese werden nacheinander in Matrizen der Form
\begin{center}
    \begin{tikzpicture}
        \draw (0,0) node[] (M) {$\small
    \begin{bmatrix}
        1 & 0 & 0 & 0 & 0 & 0 & 0\\
        \mathbf f(k+1) & 1 & 0 & 0 & 0 & 0 & 0 \\
        1 & \mathbf f(k+2) & 1 & 0 & 0 & 0 & 0 \\
        0 & 1 & \mathbf f(k+3) & 1 & 0 & 0 & 0 \\[-0.25cm]
        0 & 0 & \ddots & \ddots & \ddots & 0 & 0 \\
        0 & 0 & 0 & 1 &\mathbf f(k+n-2) & 1 & 0\\
        0 & 0 & 0 & 0 & 1 &\mathbf f(k+n-1) & 1\\
        0 & 0 & 0 & 0 & 0 & 1 &\mathbf f(k+n)\\
        0 & 0 & 0 & 0 & 0 & 0 & 1\\
    \end{bmatrix}$
    \normalsize};
    \draw[decorate,decoration={brace,amplitude=5pt},thick] ($(M.north east)-(0,0.2)$) -- ($(M.south east)+(0,0.2)$) node [black,midway,xshift=0.8cm] {$n+2$};
    \draw[decorate,decoration={brace,amplitude=5pt,mirror},thick] ($(M.south west)+(0.2,0)$) -- ($(M.south east)-(0.2,0)$) node [black,midway,yshift=-0.4cm] {$n$};
    \end{tikzpicture}
\end{center}
gesetzt und ihr kleinster Singulärwert berechnet. Danach wird das Minimum über alle diese kleinsten Singulärwerte gebildet und dadurch eine numerische Einschließung von $\norm{A^{-1}}$ errechnet. Dieses wird für verschiedenene $n$ im Folgenden dargestellt. Zu sehen ist, dass die Intervallbreite für größer werdende $n$ nur langsam sinkt. Der Grund hierfür ist, dass $\sin(\frac{1}{n}) \approx \frac{1}{n}$ für große $n$ ist, somit erfordert die Gewinnung einer weiteren Stelle etwa eine Verzehnfachung von $n$ und so mit eine erhebliche Vervielfältigung des Rechenaufwandes.
\definecolor{cred}{RGB}{190, 27, 27}
\begin{center}
    \makebox[\textwidth][c]{
        \begin{tikzpicture}
            \begin{axis}[
                title ={Schätzung an $\norm{A^{-1}}$ für $n \in \{100,110,..., 1000\}$},
                width=16cm,
                height=7cm,
                grid=both,
                no markers,
                colormap/hot,
                minor x tick num=9,
                mark=square*,
                minor y tick num=7,
                enlarge y limits=0.05,
                enlarge x limits=0.02,
                major grid style = {line width =0.3pt,black}
                ]
                \addplot+[only marks, scatter, color = cred, error bars, error mark=triangle*, error bar style={thick}, y dir=both,y  explicit] table[x=xcol, y=ycol, y error=error] {datazwnu1000.txt};
            \end{axis}
        \end{tikzpicture}
    }
\end{center}
Die beste hier berechnete Einschließung ergibt für $n=4500$:
\[4.9416990 \leq \norm{A} \leq 4.9760333\]
Dieses Verfahren lässt sich auch auf den einseitigen Fibonacci-Hamilton Operator anwenden. In einem vorherigen Kapitel wurde ja bereits bemerkt, dass der einseitige und zweiseitige Operator die selben Teilwörter besitzen, somit sind auch hier diese zu betrachten. Jedoch ist auch noch eine weitere Matrix relevant, denn die Restriktion ganz am Anfang des Operators besitzt folgende Gestalt.
\begin{center}
    \begin{tikzpicture}
        \draw (0,0) node[] (M) {$\small
    \begin{bmatrix}
        \mathbf f(1) & 1 & 0 & 0 & 0 & 0 & 0 \\
        1 & \mathbf f(2) & 1 & 0 & 0 & 0 & 0 \\
        0 & 1 & \mathbf f(3) & 1 & 0 & 0 & 0 \\[-0.25cm]
        0 & 0 & \ddots & \ddots & \ddots & 0 & 0 \\
        0 & 0 & 0 & 1 &\mathbf f(n-2) & 1 & 0\\
        0 & 0 & 0 & 0 & 1 &\mathbf f(n-1) & 1\\
        0 & 0 & 0 & 0 & 0 & 1 &\mathbf f(n)\\
        0 & 0 & 0 & 0 & 0 & 0 & 1\\
    \end{bmatrix}$
    \normalsize};
    \draw[decorate,decoration={brace,amplitude=5pt},thick] ($(M.north east)-(0,0.2)$) -- ($(M.south east)+(0,0.2)$) node [black,midway,xshift=0.8cm] {$n+1$};
    \draw[decorate,decoration={brace,amplitude=5pt,mirror},thick] ($(M.south west)+(0.2,0)$) -- ($(M.south east)-(0.2,0)$) node [black,midway,yshift=-0.4cm] {$n$};
    \end{tikzpicture}
\end{center}
Anders als bei den anderen Matrizen fehlt dieser die oberste Zeile, somit könnte diese Matrix einen noch kleineren Singulärwert besitzen und $\nu(A^+)$ noch kleiner sein, was wiederum $\norm{(A^+)^{-1}}$ vergrößern würde.
\begin{lstlisting}
n=100
v=fibham(20);
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
sv=zeros(1,n+1);

for l=1:n+1
    A(2:n+1,:)=diag(ones(1,n-1),1)+diag(Fib(l,:))+diag(ones(1,n-1),-1);
    sv(l)=min(svd(A));
end

A=zeros(n+1,n);
A(1:n,:)=diag(ones(1,n-1),1) + diag(v(1:n)) + diag(ones(1,n-1),-1);
A(n+1,n)=1;
sv(n+2)=min(svd(A));

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n));
iv=[1/(val),1/(val-delta)];
\end{lstlisting}
Dieser Code ist zum Großteil analog zum Code für den einseitigen, \texttt{fibham(20)} berechnet das Fibonacci-Wort, was dann auf die Mitte der oben beschriebenen Matrix steht. Mit diesem Code lässt sich natürlich auch gleichzeitig $\nu(A)$ berechnen, in dem das Minimum der Singulärwerte früher gebildet wird. Dieses führt zu folgenden Ergebnissen:
\begin{center}
    \makebox[\textwidth][c]{
        \begin{tikzpicture}
            \begin{axis}[
                title ={Schätzung an $\norm{(A^+)^{-1}}$ für $n \in \{100,110,..., 1000\}$},
                width=16cm,
                height=7cm,
                grid=both,
                no markers,
                colormap/hot,
                minor x tick num=9,
                mark=square*,
                minor y tick num=7,
                enlarge y limits=0.05,
                enlarge x limits=0.02,
                major grid style = {line width =0.3pt,black}
                ]
                \addplot+[only marks, scatter, color = black, error bars, error mark=triangle*, error bar style={thick}, y dir=both,y  explicit] table[x=xcol, y=ycol, y error=error] {datazwnu1000.txt};
                \addplot+[only marks, scatter, color = cred, error bars, error mark=triangle*, error bar style={thick}, y dir=both,y  explicit] table[x=xcol, y=ycol, y error=error] {dataeinnu1000.txt};
            \end{axis}
        \end{tikzpicture}
        }
\end{center}
Zu sehen ist, dass die Hinzunahme dieser einen Matrix keinen großen Unterschied zu machen scheint. In schwarz wurden nocheinmal die vorherigen Ergebnisse eingetragen, diese scheinen lediglich am Anfang abzuweichen.


\end{document}