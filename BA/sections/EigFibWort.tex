\documentclass[../main.tex]{subfiles}

\begin{document}
Dieses Wissen erlaubt uns nun eine alternative Darstellung des Fibonacci-Wortes anzugeben, in dem wieder der goldene Schnitt auftaucht.
Neben einer rekursiven Definition, wie für das Fibonacci-Wort oben, lassen sich Wörter auf dem Alphabet $\mathcal \{1, 0\}$ auch über andere Methoden darstellen, eine davon wird im Folgenden erläutert.

Zu gegebenen Werten $\alpha$ und $\rho$, wird folgende Abbildung definiert:
\[ s_{\alpha, \rho}(i) \coloneqq \chi_{[1- \alpha, 1)} ( (\alpha i + \rho) \mod 1) \quad i \in \N \]
Wobei zusätzlich im Interval die $1$ mit der $0$ identifiziert wird.

Visualisieren lässt sich die Darstellung über einen Zeiger, der sich auf einem Kreis im Uhrzeigersinn bewegt. Der Kreis ist zusätzlich in zwei Bereiche aufgeteilt: Einen, der den Anteil von $\alpha$ einnimmt (rot markiert) und einem anderen, der den Anteil $1 - \alpha$ einnimmt (blau markiert).
Der Zeiger beginnt für $i=0$ beim Startwert $\rho$ und dreht sich für die folgenden $i$ immer um $\alpha$ weiter. $s_{\alpha, \rho}(i)$ ist dann Eins, falls der Anteil der Länge $\alpha$ getroffen wird, ansonsten Null.
Dies soll in Abbildung \ref{fig:fibcirc} verdeutlicht werden.

\begin{figure}[h]
	\label{fig:fibcirc}
	\centering
	\begin{tikzpicture}[scale = 1.3]
		\draw[line width = 0.25mm] (0,0) circle (2.5);
		\draw[] (0,2.7) node[above] {$0$};
		\draw[line width = 1mm, dblue] (0,2.5) arc (90:-47.5:2.5);
		\draw[line width = 1mm, cred] (0,2.5) node[black, thick] {\textbf{\large )}} arc (-270:-47.5:2.5) node[rotate = 41.5,black, thick] { \textbf{\large ]}} node[below,xshift = 5, yshift = -5, rotate = 41.5,black, thick] {$1 - \alpha$};
		\draw[->, line width = 0.5mm, black!100] (0,2.2) arc (90:-132.5:2.2);
		\draw[->, line width = 0.5mm, rotate around={-222.5:(0,0)}, black!100] (0,1.9) arc (90:-132.5:1.9);
		\draw[->, line width = 0.5mm, rotate around={-445:(0,0)}, black!100] (0,1.6) arc (90:-132.5:1.6);
		\draw[line width = 0.3mm] (0,0) (-132.5:2.25) -- (-132.5:3) node[rotate = -42.5, below] {$s_{\alpha, \rho}(1)$};
		\draw[line width = 0.3mm] (0,0) (-355:1.95) -- (-355:3) node[rotate = -265, below] {$s_{\alpha, \rho}(2)$};
		\draw[line width = 0.3mm] (0,0) (-577.5:1.65) -- (-577.5:3) node[rotate = -487.5, below] {$s_{\alpha, \rho}(3)$};
	 \end{tikzpicture}
	 \caption{Darstellung des Fibonacci-Wortes über einen Kreis}
  \label{fig:fibcirc}
\end{figure}

Nun ergibt sich das einseitige Fibonacci-Wort, für $\alpha = \tau$ und $\rho = 0$, wobei $\tau = \frac{1}{\Phi}$, also
\[\mathbf f^+(i) = \chi_{[1-\tau,1)}((\tau i) \mod 1) \quad i \in \{1,2,...\} \]
\begin{proof}
	Der Beweis hierzu befindet sich in \cite{Algebraic} und \cite{Dennis}.
\end{proof}

In obiger Definition lassen sich nun jedoch nicht nur die positiven ganzen Zahlen, sondern auch die Null und alle negativen ganzen Zahlen einsetzen.
Somit erhält man auch das zweiseitige Fibonacci-Wort.

\begin{definition}[Zweiseitiges Fibonacci-Wort]
	\[\mathbf f(i) \coloneqq \chi_{[1 - \tau,1)}((\tau i) \mod 1) \quad i \in \Z \]
\end{definition}

Dieses zweiseitige Wort besitzt einen essentiellen Zusammenhang zum einseitigen Wort, der im nächsten Satz festgehalten wird.

\begin{theorem}
	Für das Zweiseitige Fibonacci-Wort gilt
	\[\mathbf f = ( \mathbf f^+)^R \circ 1 \circ 0 \circ \mathbf f^+.\]
\end{theorem}

\begin{proof}
	Zu zeigen ist also, dass $\mathbf f^+(i)=\mathbf f^+(-i-1)$ für $i>1$, wobei aus dem Fakt, dass $f^+(i) \in \{0,1\}$ folgt, dass es ausreicht
	\[\mathbf f^+(1)=1 \iff \mathbf f^+(-i-1)=1 \quad i>1 \]
	zu beweisen. Zunächst wird jedoch gezeigt, dass
	\[(-x) \mod 1 = 1 - (x \mod 1) \quad x \in \R.\]
	Dies gilt, da
	\[(-x) \mod 1 = \left \lceil x \right \rceil - x = 1 - (\underbrace{1 - \left \lceil x \right \rceil}_{-\left \lfloor x \right \rfloor} + x) = 1 - (-\left \lfloor x \right \rfloor + x) = 1 - (x \mod 1).\]
	Nun gilt also, zusammen mit $(\tau i) \mod 1 \neq 0$ auf Grund der Irrationalität von $\tau$
	\begin{align*}
		\mathbf f^+(i)=1 & \iff (\tau i) \mod 1 < 1 - \tau \\
		& \iff (\tau(i+1)) \mod 1 > \tau \\
		& \iff 1 - (-\tau (i+1)) \mod 1 >  \tau \\
		& \iff (-\tau (i+1)) \mod 1 < 1-\tau \\
		& \iff \mathbf f^+(-i-1)=1.
	\end{align*}
\end{proof}

Später werden die Faktoren des zweiseitig unendlichen Fibonacci-Wortes eine große Rolle spielen. Zu der Anzahl der Faktoren des Fibonacci-Wortes existiert ein wichtiger Satz und die Berechnung dieser kann schnell mit einem Algorithmus aus \cite{Dennis} durchgeführt werden.

\begin{theorem}
    Das einseitige Fibonacci-Wort $\mathbf f$ ist ein \mim{Sturmsches Wort}, besitzt genau $n+1$ Faktoren der Länge $n$.
\end{theorem}

\begin{proof}
    Nicht hier, der Beweis findet sich in \cite{Algebraic}.
\end{proof}

Außerdem gilt, dass $\mathcal F(\mathbf f) = \mathcal F(\mathbf f^+)$. Der Beweis hierfür findet sich etwa in \cite{Hagen}, somit ist nicht nur $\mathbf f^+$ ein Sturmsches Wort, sondern auch $\mathbf f$.

Zum Thema von Sturmschen Wörtern findet sich mehr in \cite{Algebraic}.

\end{document}