\documentclass[../main.tex]{subfiles}
 
\begin{document}

Eine bestimmte Art der endlichen Matrizen oder Operatoren sind die Bandoperatoren.
\begin{definition}[Bandoperator]
	Sei $A=(a_{ij}) : \ell^p \to \ell^p$ ein Operator, dann heißt dieser ein Bandoperator, falls ein $b \in \N$ existiert, sodass
	\[ \abs{i-j} > d  \Rightarrow a_{ij}=0.\]
	In Worten bedeutet dies, dass außerhalb von Einträgen, die einen Abstand $d$ von der Diagonalen haben, alle Einträge Null sind.
\end{definition}

Betrachtet man nun einen solchen ein- oder zweiseitigen Bandoperator, dann ist die Idee eines Grenzoperators zu einer gegebenen Folge für die Eigenschaften der FSM, angewendet auf diesen Operator, von Bedeutung, mehr dazu in \cite{Coburn}.

\begin{definition}[Grenzoperator]
	Für einen ein- oder zweiseitig unendlichen Operator $A=(a_{ij})_{i,j \in M}$, wobei $M=\N$ oder $M=\Z$ und eine Folge $h_n \in M$, mit $\abs{h_n} \to \infty$, ist $B=(b_{ij})_{i,j \in \Z}$ ein Grenzoperator von $A$ falls für all $i,j \in \Z$
	\[a_{i+h_n,j+h_n} \to b_{ij} \text{ für } n \to \infty\]
	Für die Menge der Grenzoperatoren von $A$ bezüglich einer Teilfolge $h$ von $g$ schreiben wir $\text{Lim}_g(A)$. Weiterhin definieren wir $\text{Lim}(A)$ für die Menge aller Grenzoperatoren und $\text{Lim}_+(A)\coloneqq \text{Lim}_{(1,2,3,...)}(A)$ so wie $\text{Lim}_-(A)\coloneqq \text{Lim}_{(-1,-2,-3,...)}(A)$
\end{definition}

Das Zumindest ein solcher Grenzoperator immer existiert, ist begründet dadurch, dass jede der Diagonalen der Matrix beschränkt ist. Denn zusammen mit dem Satz von Bolzano-Weierstrass, der besagt, dass jede beschränkte Folge von reellen Zahlen eine konvergente Teilfolge besitzt und der Tatsache, dass aus der Bandstruktur der Matrix folgt, dass es nur endlich viele Diagonalen gibt, lässt sich mit einem Teilfolgenargument schließen, dass eine Folge existiert, sodass punktweise Konvergenz garantiert ist.

Weiterhin definieren wir die positiven und negativen Eck-Grenzoperatoren.
\begin{definition}[Eck-Grenzoperatoren]
	Man betrachte wieder einen einseitig oder zweiseitig unendlichen Operator $A=(a_{ij})_{i,j \in M}$, wobei $M=\N$ oder $M=\Z$, zusammen mit Abschneide Folgen $l_n$ und $r_n$(im einseitgen Fall wird $l_n=1$ gesetzt). Dann sind 
	\[(a_{i + l_n', j+ l_n'})_{i,j=0}^\infty \to B_+ \text{ und }(a_{i + r_n', j+ r_n'})^0_{i,j=-\infty} \to C_- \text{ wobei } n \to \infty\]
	Eck-Grenzoperatoren zu den Teilfolgen $l_n'$ von $l_n$ bzw. $r_n'$ von $r_n$.
\end{definition}

Es gibt nun einige Zusammenhänge zwischen diesen Grenzoperatoren und der Stabilität bzw. Anwendbarkeit der FSM, nämlich gilt:

\begin{theorem}[siehe \cite{Lindner}]
	Sei $A=(a_{ij})_{i,j \in \Z}$ ein invertierbarer Bandoperator auf $\ell^p(\Z)$, zusammen mit zwei Abschneidefolgen $(r_n)_{n \in \N}, \ r_n \to \infty$ und $(l_n)_{n \in \N}, \ l_n \to \infty$. Weiterhin sei $A_n \coloneqq (A_{ij})_{i,j=l_n}^{r_n}$ wieder die Folge der endlichen Ausschnitte für die FSM, dann sind folgende Aussagen äquivalent.
	\begin{enumerate}
			\item Die FSM ist für die Operatorfolge $(A_n)$ anwendbar
			\item Die Folge $(A_n)$ ist stabil 
			\item Die Grenzoperatoren $B_+$ und $C_-$, wobei $B \in \text{\normalfont Lim}_+(A)$ und $C \in \text{\normalfont Lim}_-(A)$ sind invertierbar.
	\end{enumerate}
\end{theorem}

Um ein Beispiel für dieses Theorem zu geben, wird erneut der Operator
\[A=
    \left[
    \begin{array}{ccccccccccc}
    \ddots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \reflectbox{$\ddots$} \\[-0.1mm]
    \cdots & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & \cdots \\
    \cdots & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \cdots \\
    \cdots & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & \cdots \\
    \cdots & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & \cdots \\
    \cdots & 0 & 0 & 0 & 0 & $\boxed{0}$ & 1 & 0 & 0 & \cdots \\
    \cdots & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & \cdots \\
    \cdots & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & \cdots \\
    \cdots & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & \cdots \\
    \reflectbox{$\ddots$} & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \ddots \\
    \end{array}
    \right],
\]
betrachtet. Wie bereits festgestellt, liegt für die Wahl $l_n=-2n$ und $r_n=2n-1$ Stabilität vor. Also werden nun die Eck-Grenzoperatoren betrachtet. Per Definition gilt, dass
\[B_+ = \lim_{n \to \infty} (a_{i + l_n, j + l_n})_{i,j=0}^\infty.\]
Da die Diagonale von $A$, sowie alles außerhalb der beiden Nebendiagonalen, konstant 0 ist, ergibt sich, dass dieses für unseren Eck-Grenzoperator auch gelten muss. Durch Anwenden der Definition des Grenzoperators ergeben sich für den linken bzw rechten Eck-Grenzoperator folgende Operatoren.
\begin{center}
	\begin{tikzpicture}
		\matrix (M) [inner sep=0pt,nodes={rectangle, inner sep=3pt,outer sep=0pt,text depth=0cm, text height = 10pt, text width = 10pt, anchor=center,left},matrix of math nodes, left delimiter={[}, right delimiter={]}]
			{
				0 & 1 & 0 & 0 & \hdots\\
				1 & 0 & 0 & 0 & \hdots\\
				0 & 0 & 0 & 1 & \hdots\\
				0 & 0 & 1 & 0 & \ \ddots\\
				\vdots & \vdots  & \vdots & \ddots & \ddots \\
			};
			\draw[] (M.west) node[left,xshift=-0.75cm] {$B_+=$};
	
			\draw[rounded corners=3pt,xshift=-1cm] (M-1-1.north west) rectangle (M-2-2.south east);
			\draw[rounded corners=3pt] (M-3-3.north west) rectangle (M-4-4.south east);
	
		\matrix (M2) [inner sep=0pt,nodes={rectangle, inner sep=3pt,outer sep=0pt,text depth=0cm, text height = 10pt, text width = 10pt, anchor=center,left},matrix of math nodes, left delimiter={[}, right delimiter={]}, right =of M, xshift = 1cm]
			{
				\ddots & \ddots & \vdots & \vdots & \vdots\\
				\ddots & 0 & 1 & 0 & 0\\
				\hdots & 1 & 0 & 0 & 0\\
				\hdots & 0 & 0 & 0 & 1\\
				\hdots & 0 & 0 & 1 & 0\\
			};
			\draw[] (M2.west) node[left,xshift=-0.15cm] {$C_-=$};
	
			\draw[rounded corners=3pt] (M2-2-2.north west) rectangle (M2-3-3.south east);
			\draw[rounded corners=3pt] (M2-4-4.north west) rectangle (M2-5-5.south east);
	\end{tikzpicture}
\end{center}

Wie zu sehen ist, sind diese Operatoren beide invertierbar, was unser Theorem bestätigt. Hätte man jetzt $l_n=-2n-1$ und $r_n=2n$ gewählt, die Folge, für die wie bereits festgestellt, keine Stabilität vorliegt, würde man auch andere Eck-Grenzoperatoren erhalten, wobei die Berechnung wieder analog zu dem oben Beschriebenen erfolgt. Die Eck-Grenzoperatoren ergeben sich zu:

\begin{center}
	\begin{tikzpicture}
		\matrix (M) [inner sep=0pt,nodes={rectangle, inner sep=3pt,outer sep=0pt,text depth=0cm, text height = 10pt, text width = 10pt, anchor=center,left},matrix of math nodes, left delimiter={[}, right delimiter={]}]
			{
				0 & 0 & 0 & 0 & \hdots\\
				0 & 0 & 1 & 0 & \hdots\\
				0 & 1 & 0 & 0 & \hdots\\
				0 & 0 & 0 & 0 & \ \ddots\\
				\vdots & \vdots  & \vdots & \ddots & \ddots \\
			};
			\draw[] (M.west) node[left,xshift=-0.75cm] {$B_+=$};
	
			\draw[rounded corners=3pt,xshift=-1cm] (M-2-2.north west) rectangle (M-3-3.south east);

		\matrix (M2) [inner sep=0pt,nodes={rectangle, inner sep=3pt,outer sep=0pt,text depth=0cm, text height = 10pt, text width = 10pt, anchor=center,left},matrix of math nodes, left delimiter={[}, right delimiter={]}, right =of M, xshift = 1cm]
			{
				\ddots & \ddots & \vdots & \vdots & \vdots\\
				\ddots & 0 & 0 & 0 & 0\\
				\hdots & 0 & 0 & 1 & 0\\
				\hdots & 0 & 1 & 0 & 0\\
				\hdots & 0 & 0 & 0 & 0\\
			};
			\draw[] (M2.west) node[left,xshift=-0.15cm] {$C_-=$};
	
			\draw[rounded corners=3pt] (M2-3-3.north west) rectangle (M2-4-4.south east);
	\end{tikzpicture}
\end{center}
Diese Operatoren sind nicht invertierbar, was noch einmals das obige Theorem bestätigt.

Weiterhin besteht ein wichtiger Zusammenhang zwischen dem $\limsup_{n \to \infty}\norm{A_n^{-1}}$ und den Eck-Grenzoperatoren, denn es gilt:

\begin{theorem}[siehe \cite{Hagger}]\label{thm:limsupgrz}
	Sei $A:\ell^p(\Z) \to \ell^p(\Z)$ ein Operator und $A_n$ die Folge, der durch Anwendung der FSM entstandenden Matrizen, dann gilt
	\[\limsup_{n \to \infty}\norm{A_n^{-1}}= \max_{B \in \text{\normalfont Lim}_+(A),C \in \text{\normalfont Lim}_-(A)}\{\norm{A^{-1}}, \norm{B_+^{-1}}, \norm{C_-^{-1}}\}.\]
	Wobei auf der rechten Seite das Maximum nicht nur über drei Operatoren, sondern über alle Grenzoperatoren gebildet wird.
\end{theorem}

Ein ähnlicher Satz gilt auch für einseitige Operatoren.

\begin{theorem}\label{thm:limsupgre}
	Sei $A^+:\ell^p(\N) \to \ell^p(\N)$ ein Operator und $A_n^+$ die Folge der durch Anwendung der FSM entstandenden Matrizen, dann gilt
	\[\limsup_{n \to \infty}\norm{(A_n^+)^{-1}}= \max_{B\in \text{\normalfont Lim}(A^+)}\{\norm{(A^+)^{-1}}, \norm{B_+^{-1}}\}.\]
\end{theorem}
Wobei sich dieses Theorem analog beweisen lässt.

\end{document}