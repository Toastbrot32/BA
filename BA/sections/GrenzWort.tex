\documentclass[../main.tex]{subfiles}

\begin{document}

Betrachtet man lediglich die Diagonale einer unendlichen Matrix, so ergibt sich die Idee eines Grenzwortes. Dies ist besonders relevant, da der Fibonacci-Hamilton-Operator auf den Nebendiagonalen konstant ist. Somit sind auch die Nebendiagonalen der (Eck-)Grenzoperatoren schon bestimmt, also ist lediglich die Diagonale der (Eck-)Grenzoperatoren von Interesse.

In diesem gesamten Abschnitt werden lediglich Worte über dem Alphabet $\mathcal A = \{0,1\}$ betrachtet.

\begin{definition}[Grenzwort]
	$\mathbf v$ ist ein Grenzwort des ein- oder zweiseitig unendlichen Wortes $\mathbf w$, falls für eine Folge $(h_n)_{n \in \N}$ mit $\abs{h_n} \to \infty$
	\[\lim_{n \to \infty} \mathbf w(i + h_n) = \mathbf v(i), \forall  i \in \Z \]
	gilt.
\end{definition}

Um dieses Konzept näher zu erläutern, wird wieder das Wort $\mathbf w = \wort{0, 1, 0, 1, 0, 1, ... } \in \mathcal A^{\N}$ betrachtet. Für $(h_n)_{n \in \N}$ wählen wir zunächst die Folge $h_n=2 \cdot n$, also die Folge $2,4,6,8,...$ \ .

Im Folgenden schreiben wir $\mathbf w_n$ für das Wort $\mathbf w$, beginnend ab der $n$-ten Stelle, also formal
\[\mathbf w_n(i) \coloneqq \mathbf w(i+n) \quad i \in \N_0.\]
Natürlich gilt $\mathbf w_0 = \mathbf w$.
\setlength{\tabcolsep}{2pt}
\begin{center}
	\begin{tabular}{c|p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}>{\columncolor{lred}}p{10pt}>{\columncolor{lblue}}p{10pt}>{\columncolor{lgreen}}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}}
		\cellcolor[gray]{0.9} \ Stelle  \ & -6 & -5 & -4 & -3 & -2 & -1 & 0 & 1 & 2 & 3 & 4 & 5 & 6 \\\hline\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_0$ \ & \ & \ & \ & \ & \ & $\leftarrow$ & [ & 0, & 1, & 0, & 1, & 0,           & 1, & ... & ]\\ \hline
		\cellcolor[gray]{0.9} \ $\mathbf w_2$ \ & \ & \ & \ & $\leftarrow$ & [ & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... & ]\\ \hline
		\cellcolor[gray]{0.9} \ $\mathbf w_4$ \ & \ & $\leftarrow$ & [ & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... \ &]\\ \hline
		\cellcolor[gray]{0.9} \ $\mathbf w_6$ \ & [ & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... \ &]\\
	\end{tabular}
\end{center}
\setlength{\tabcolsep}{6pt}

Hier wird dargestellt wie etwa ein Grenzwort entstehen könnte. Als Folge $h_n$ haben wir ja genau $h_n=2 \cdot n$ gewählt, wir verschieben also $\mathbf w$ in jedem Schritt um $2$ Stellen nach hinten. Die $i$-te Stelle des Grenzwortes ergibt sich nun, wenn man eine der Spalten betrachtet, denn genau der Grenzwert der Elemente dieser Spalte ist der Wert des Grenzwortes an dieser Stelle.\\
Betrachten wir also nun die hier rot markierte Spalte, welche die erste Spalte des Grenzwortes beschreibt, ist zu sehen, dass diese Spalte konstant 0 ist, also ist $\mathbf v(1)=0$. Mit der gleichen Logik folgt auch das $\mathbf v(2)=1$ (blaue Spalte) und $\mathbf v(3)=0$ (grüne Spalte). Insgesamt folgt also, dass das Grenzwort $\mathbf v$ genau das Wort selbst, also $\mathbf w$ ist.

Dieses ist jedoch nicht immer der Fall. Betrachten wir eine andere Folge, diesmal $h_n=2\cdot n+1$, ergibt sich ein anderes Grenzwort, dieses wird in der folgenden Grafik dargestellt.

\setlength{\tabcolsep}{2pt}
\begin{center}
	\begin{tabular}{c|p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}>{\columncolor{lred}}p{10pt}>{\columncolor{lblue}}p{10pt}>{\columncolor{lgreen}}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}}
		\cellcolor[gray]{0.9}\ Stelle \ &-7&-6&-5&-4&-3&-2&-1&0&1&2&3&4&5\\\hline\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_1$ \ & \ & \ & \ & \ & \ & $\leftarrow$ & [ & 0, & 1, & 0, & 1, & 0, & 1, & ... & ]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_3$  \ & \ & \ & \ & $\leftarrow$ & [ & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... & ]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_5$ \ & \ & $\leftarrow$ & [ & 0,& 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... &]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_7$ \ &[ & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & 0, & 1, & ... &]\\

	\end{tabular}
\end{center}
\setlength{\tabcolsep}{6pt}

Zu sehen ist, dass die erste Stelle, hier wieder rot markiert, konstant 1 ist. Die erste Stelle des Grenzwortes ist also 1 und somit folgt $\mathbf v(1)=1$. Wie bereits oben beschrieben, folgt weiter, dass $\mathbf v(2)=0$ und $\mathbf v(3)=1$. Somit ist das Grenzwort diesmal nicht $\mathbf w$, sondern $\mathbf w_1$.

Noch etwas anderes passiert wenn man als Folge $h_n=n$ wählt, hier wieder die Grafik.
\setlength{\tabcolsep}{2pt}
\begin{center}
	\begin{tabular}{c|p{10pt}p{10pt}p{10pt}p{10pt}>{\columncolor{lred}}p{10pt}>{\columncolor{lblue}}p{10pt}>{\columncolor{lgreen}}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}p{10pt}}
		\cellcolor[gray]{0.9} \ Stelle&-3&-2&-1&0&1&2&3&4&5&6\\\hline\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_0$ \ & \ & \ &$\leftarrow$&[&0,&1,&0,&1,&0,&1,&...&]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_1$ \ & \ & $\leftarrow$&[&0,&1,&0,&1,&0,&1,&0,&...&]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_2$ \ & $\leftarrow$&[&0,&1,&0,&1,&0,&1,&0,&1,&...\ &]\\\hline
		\cellcolor[gray]{0.9} \ $\mathbf w_3$ \ & [&0,&1,&0,&1,&0,&1,&0,&1,&0,&...\ &]\\
	\end{tabular} 
\end{center}
\setlength{\tabcolsep}{6pt}

Betrachtet man jetzt die rot markierte Folge, welche die erste Stelle beschreibt, sieht man, dass bei dieser keinerlei Konvergenz vorliegt, da sie zwischen 1 und 0 alterniert. Das Gleiche gilt für alle anderen Stellen. Also ergibt sich für diese Wahl von $h_n$ kein Grenzwort.

Dieses Konzept lässt sich analog auch auf zweiseitig unendliche Wörter übertragen.

Weiter ist zu bemerken, dass die Worte, die hier betrachtet werden, über dem Alphabet $\mathcal A=\{0,1\}$ definiert sind. Daraus folgt auch, dass die Konvergenz einer Folge von Elementen aus $\mathcal A$ bedeutet, dass diese konstant ab einem bestimmten Punkt sein muss. Dies führt auf folgendes Lemma.

\begin{lemma}\label{lem:grenz}
	Sei $\mathbf v$ ein Grenzwort von $\mathbf w$ bezüglich der Folge $(h_n)_{n \in \N}$, wobei $\abs{(h_n)_{n \in \N}} \to \infty$, dann folgt, dass
	\[\forall i \in \Z \ \exists n_i : \forall n \geq n_i \lim_{m \to \infty} \mathbf w(i + h_m) = \mathbf w(i + h_{n})= \mathbf v(i).\]
	Mit anderen Worten, die Folge $(\mathbf w(i + h_m))_{m \in \N}$ ist ab einem gewissen $n_i$ konstant. Auch definieren wir
	\[n_i^{max} \coloneqq \max_{\abs{j}\leq i} \ \{n_{j}\}.\]
	Dieses $n_i^{max}$ ist also die Konstante, für die das Grenzwort $\mathbf v(i)$ für $\abs{j}\leq i$ Schritte nach links und rechts als $\mathbf w(i+h_{n_i^{max}})$, also ohne den Limes, geschrieben werden kann.
\end{lemma}
\begin{proof}
	Sei $i \in \Z$ beliebig, es gilt
	\[\lim_{n \to \infty} \mathbf w(i + h_n) = \mathbf v(i).\]
	Dies bedeutet per Definition
	\[\forall \epsilon>0  \ \exists n_i \in \N \ \forall n \geq n_i : \abs{\mathbf v(i) - \mathbf w(i + h_n)} < \epsilon.\]
	Da der Betrag der Differenz zweier Zahlen, die entweder 1 oder 0 sind, auch entweder 1 oder 0 ist, folgt, dass für $\epsilon = 0.5$ ein $n_i$ existiert, sodass für alle $n \geq n_i$ $\abs{\mathbf v(i) - \mathbf w(i + h_n)}=0$ ist. Also ist
	\[\lim_{n \to \infty} \mathbf w(i + h_n) = \mathbf w(i + h_{n_i})=v(i).\]
\end{proof}




\end{document}