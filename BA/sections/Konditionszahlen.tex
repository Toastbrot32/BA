\documentclass[../main.tex]{subfiles}
 
\begin{document}
Für viele mathematische Probleme, die an einem Computer gelöst werden sollen, ist von Bedeutung, in wiefern sich das gewünschte Resultat durch Störungen der Eingabedaten verändert. Die Gründe für solche Störungen können vielfältig sein und nicht nur durch Verwendung von imperfekten Messdaten entstehen, sondern auch durch die Darstellung von Zahlen an einem Computer, welche in den meisten Fällen ungenau ist.\\
Sehr häufig basiert Computer Arithmetik auf Gleitpunkt-Arithmetik, auf Englisch floating point Arithmetic; bei dieser muss prinzipiell damit gerechnet werden, dass Fehler der Größe $\approx 10^{-16}$ bei den meisten Berechnungen auftreten, einfach aufgrund der Darstellung von diesen Zahlen an einem Computer.\\ %!!!Higham?
Etwa könnte für die Gleichung
\[Ax=b,\]
wobei $A \in \R^{n \times n}$ und $b \in \R^{n}$ gegeben sind und $x$ die Lösung des Problems ist, $b$ um $\Delta b$ gestört sein. Anstatt unser eigentliches Problem zu lösen, lösen wir in Wirklichkeit das Problem
\[A \tilde x = b + \Delta b.\]
Nun ergibt sich die Frage, wie $x$ und $\tilde x$ zusammenhängen; dieser Zusammenhang entscheidet darüber, wie nahe wir an die echte Lösung unseres Problems, selbst bei fehlerhaften Eingabedaten, herankommen können. Zunächst betrachten wir hierzu das gestörte Problem:
\[A \tilde x = b + \Delta b\]
Und ersetzen $b$ durch $Ax$ und erhalten somit
\[A \tilde x = Ax + \Delta b \Rightarrow A \tilde x - Ax =  A( \tilde x - x) = \Delta b.\]
Die Antwort auf die Frage des Zusammenhanges, zwischen $x$ und $\tilde x$ lautet also, dass der absolute Fehler durch
\[(\tilde x-x) = A^{-1} \Delta b\]
gegeben ist. Wollen wir nun die Norm dieses Unterschiedes bestimmen, erhalten wir
\[ \norm{( \tilde x-x)} = \norm{A^{-1} \Delta b} \leq \norm{A^{-1}} \cdot \norm{\Delta b}.\]
Essentiell zur Bestimmung, wie gut wir ein Problem, trotz Fehlern, lösen können, ist also der Faktor $\norm{A^{-1}}$. Neben dem absoluten Fehler, ist auch der relative Fehler, also $\frac{\norm{\tilde x - x}}{\norm{x}}$ von Bedeutung. Um diesen zu bestimmen, benutzen wir das $\norm{b}= \norm{Ax} \leq \norm{A} \cdot \norm{x}$ und folgern daraus
\[ \frac{\norm{\tilde x - x}}{\norm{x}} \leq \norm{A} \cdot \norm{A^{-1}} \cdot \frac{\norm{\Delta b}}{\norm{b}}.\]

\begin{figure}[h]
	\centering
	\begin{tikzpicture}
		\draw[rounded corners=15pt,line width=0.4mm] (-5,0) rectangle (-1.5,-4);
		\draw[line width=0.4mm] (-3.25,-0.5) node[] {$\R^n$};
		\draw[line width=0.4mm,rounded corners=15pt] (5,0) rectangle (1.5,-4);
		\draw[line width=0.4mm] (3.25,-0.5) node[] {$\R^n$};
		\draw[line width=0.3mm] (-3.25,0.1) edge[bend left,->] node[above] {$A$} (3.25,0.1);
		\draw[line width=0.4mm] (4,-1.5) node[circle,fill,inner sep=0.5pt] {} node[right] {$b$};
		\draw[line width=0.2mm] (3.5,-3) node[circle,fill,inner sep=0.5pt] {} node[right] {$b + \Delta b$};
		\draw[line width=0.2mm] (4,-1.5) -- (3.5,-3);
		\draw[line width=0.2mm] (-3,-1.25) node[circle,fill,inner sep=0.5pt] {} node[left] {$x$};
		\draw[line width=0.2mm] (-3.25,-3) node[circle,fill,inner sep=0.5pt] {} node[left] {$\tilde x$};
		\draw[line width=0.2mm] (-3,-1.25) -- (-3.25,-3);
		\draw[line width=0.3mm] (3.9,-1.5) edge[->,bend right = 10] node[sloped,above] {$A^{-1}b$} (-2.9,-1.2);
		\draw[line width=0.3mm] (3.4,-3) edge[->,bend left = 10] node[sloped,above] {$A^{-1}(b+ \Delta b)$} (-3.15,-3);
	\end{tikzpicture}
	\caption{Darstellung der Konditionszahl}
	\label{fig:cond}
\end{figure}

\begin{definition}[Konditionszahl]
	Für das Problem $Ax=b$ ist die Konditionszahl über
	\[\kappa(A)\coloneqq \norm{A} \cdot \norm{A^{-1}}\]
	definiert.
\end{definition}

Für die FSM besonders relevant ist, wie sich die Folge von Konditionszahlen verhält. Damit eine Folge stabil ist, wird verlangt, dass $\sup_{n > n_0}\norm{A_n^{-1}} < \infty$, jedoch nicht, dass $\sup_{n > n_0}\norm{A_n} < \infty$, obwohl beide ja zunächst gleich wichtig zu sein scheinen. Das folgende Theorem schafft darüber Klarheit.

\begin{theorem}\label{thm:kond}
	Falls die FSM angewendet auf einen Operator $A$, stabil für die Folge von endlich dimensionalen Matrizen $A_n$, ist, wobei Invertierbarkeit ab einem $n_0 \in \N$ gilt, dann folgt mit \cite{Hagger}, dass
	\[\limsup_{n \geq n_0} \norm{A_n} = \lim_{n \to \infty} \norm{A_n} = \norm{A}.\]
	Und mit dieser Tatsache, auch dass
	\[\limsup_{n \geq n_0} \kappa(A_n) = \norm{A} \cdot \limsup_{n > n_0}\norm{A_n^{-1}}\]
	gilt.
\end{theorem}

\begin{proof}
	In \cite{Hagger} wird gezeigt, dass $\norm{A} \leq \liminf_{n \geq n_0} \norm{A_n}$ somit ergibt sich zusammen mit der punktweisen Konvergenz von $A_n \to A$ und der Tatsache, dass $\norm{A_n} \leq \norm{A}$, dass
	\[\norm{A} \leq \liminf_{n \geq n_0} \norm{A_n} \leq \limsup_{n \geq n_0} \norm{A_n} \leq \norm{A}\]
	gilt. Also genau das $\lim_{n \to \infty} {A_n}$ existiert und gleich $\norm{A}$ ist.\\
	Weiterhin gilt per Definition, dass
	\[\limsup_{n \geq n_0} \kappa(A_n) = \limsup_{n \geq n_0} (\norm{A_n} \cdot \norm{A_n^{-1}})\]
	und da bereits gezeigt wurde, dass sowohl $\limsup_{n \geq n_0} \norm{A_n}$ als auch $\limsup_{n \geq n_0} \norm{A_n^{-1}}$ existieren, folgt
	\[\limsup_{n \geq n_0} (\norm{A_n} \cdot \norm{A_n^{-1}}) = \limsup_{n \geq n_0} \norm{A_n} \cdot \limsup_{n \geq n_0} \norm{A_n^{-1}} = \norm{A} \cdot \limsup_{n \geq n_0} \norm{A_n^{-1}}.\]
\end{proof}

Dieser Satz zeigt, dass für das Verhalten der Konditionszahl hauptsächlich das Verhalten von $\norm{A_n^{-1}}$ von Relevanz ist. Im Allgemeinen gilt auch, dass wirklich keine Konvergenz von $\norm{A_n^{-1}} \to \norm{A^{-1}}$ vorliegt, ein Beispiel dafür wird später auftreten.

\end{document}