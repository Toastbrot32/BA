\documentclass[../main.tex]{subfiles}

\begin{document}
Ein wesentlicher Bestandteil vieler mathematischer Probleme sind die Operatoren, eine Verallgemeinerung der bekannten Matrizen, die auch auf unendlich dimensionalen Vektorräumen existieren. Mehr zu diesem Thema findet sich etwa in \cite{Werner}.

\begin{definition}[Operator]
	Ein Operator ist eine Abbildung zwischen zwei $\mathbb K$-Vektorräumen, $T:X \to Y$ für die gilt
	\[T(\alpha(x + y)) = \alpha T(x) + \alpha T(y) \quad x,y \in X, \alpha \in \mathbb K.\]
	Anstelle von $T(x)$ wird oft nur $Tx$ geschrieben, was der Schreibweise für die Matrix-Vektor-Multiplikation ähnelt, die hier verallgemeinert werden soll.
\end{definition}

Für einen Operator, sowohl im endlich dimensionalem Matrix Fall, als auch im unendlichen, spielt seine \mim{Norm} eine wesentliche Rolle.
Man stelle sich im Zwei-dimensionalen Falle einen Kreis um den Ursprung vor. Durch das Anwenden eines Operators, in diesem Falle eine $2 \times 2$ Matrix, wird dieser Kreis auf eine Ellipse verformt. Nun haben nicht mehr alle Punkte dieser Ellipse den selben Abstand zum Mittelpunkt, wie es vorher beim Kreis der Fall war; der Punkt mit dem größten Abstand ist der, den wir als Norm bezeichnen.
Diese Zusammenhänge sind in einer folgenden Grafik \ref{fig:OpNorm} noch einmal dargestellt.

Die Norm ist also eine Kennzahl, die beschreibt, wie stark der Operator Vektoren, die er abbildet, streckt. Mathematisch ausgedrückt ergibt sich folgende Definition.

\begin{definition}[Norm]
	Sei $T:X \to Y$ ein Operator zwischen den normierten Vektorräumen $X$ und $Y$, seine Norm $\norm{T}$ wird wie folgt definiert.
	\[\norm{T} \coloneqq \sup_{x \in X,\norm{x} \leq 1} \norm{Tx} \]
\end{definition}

Die Norm eines Operators besitzt diverse interessante Eigenschaften, eine wichtige wird im Folgenden genannt.

\begin{theorem}[Normeigenschaften]
	Sei $T:X \to Y$ ein Operator und $x \in X$, dann gilt 
	\[\norm{Tx} \leq \norm{T} \norm{x}.\]
\end{theorem}
\begin{proof}
	\[\norm{T} = \sup_{x \in X,\norm{x} \leq 1} \norm{Tx} = \sup_{x \in X, x \neq 0} \frac{\norm{Tx}}{\norm{x}} \geq \frac{\norm{Tx}}{\norm{x}}\]
	Der Fall $x=0$ ist auch klar.
\end{proof}


Diese, bisher nur als solche bezeichnete, Norm ist tatsächlich eine Norm auf der Menge der Operatoren von $X$ nach $Y$. Dieses wird etwa in \cite{Werner} bewiesen.

In der obigen Beschreibung der Norm, durch die Verformung eines Kreise, kann uns auch der kleinste Abstand zwischen Oval und Mittelpunkt interessieren, diesen bezeichnen wir als lower Norm.

\begin{definition}[lower Norm]\label{def:lownorm}
	Sei nun wieder $T:X \to Y$ ein Operator, dann ergibt sich die lower Norm, geschrieben als $\nu(T)$ über folgende Definition.
	\[\nu(T) = \inf_{x \in X, \norm{x} \leq 1} \norm{Tx}_Y \]
\end{definition}

Anders als die eigentliche Norm, ist die lower Norm \textit{keine} Norm. Denn Operatoren, die nicht injektiv sind, bilden nicht Null Vektoren auf die Null ab was bedeutet, dass diese Operatoren eine lower Norm von $0$ besitzen, obwohl sie nicht notwendigerweise alle Vektoren auf Null abbilden.

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[scale = 1.5]
		\draw[->, line width = 0.5mm] (-2,-1.5) -- (-2,1.5);
		\draw[->, line width = 0.5mm] (-3.5,0) -- (-0.5,0);
		\draw[line width = 0.3mm] (-2,0) circle (1);
		\draw[->, line width = 0.4mm] (-2,0) --  node[below, sloped] {$1$} ++(66:1);
		\draw[->, line width = 0.5mm] (2,-1.5) -- (2,1.5);
		\draw[->, line width = 0.5mm] (0.5,0) -- (3.5,0);
		\draw[rotate around = {37:(2,0)}, line width = 0.3mm] (2,0) ellipse (1.3 and 0.9);
		\draw[->, dblue, line width = 0.4mm] (2,0) -- node[sloped, below, black] {$\norm{T}$} ++(37:1.3);
		\draw[->, cred, line width = 0.4mm] (2,0) -- node[sloped, below, black] {$\nu(T)$} ++(127:0.9);
		\draw[line width = 0.4mm] (-1,0.75) edge[bend left,->] node[sloped,above] {$x \mapsto Tx$} (1,0.75); 
	\end{tikzpicture}
	\caption{Darstellung der Norm und lower Norm eines Operators}
	\label{fig:OpNorm}
\end{figure}

Zwischen Norm, lower Norm und den Singulärwerten einer Matrix besteht ein wichtiger Zusammenhang. Dieser Zusammenhang erlaubt es die Norm einer Matrix zu berechnen, ohne das potentiell nur sehr schwer zu lösbare Minimierungsproblem aus der Definition zu lösen. 

\begin{theorem}\label{thm:sv}
	Sei $M:\R^n \to \R^m$ eine Matrix. Dann gilt:
	\begin{enumerate}[label=\textbullet]
		\item $\norm{M}_2=\sigma_{max}$
		\item $\nu(M)=\sigma_{min}$
	\end{enumerate}
	Wobei auch Null Singulärwerte betrachtet werden, welche im Fall der nicht Invertierbarkeit von $M^T M$ auftreten.
\end{theorem}

\begin{proof}
	Sei die Singulärwertszerlegung von $M$ gegeben als $M = U \Sigma V^T$. Da $U$ eine orthogonale Matrix ist gilt $U^TU=I$. Also ergibt sich die Singulärwertszerlegung von $M^TM$ zu
	\[M^T M = V \Sigma^T U^T U \Sigma V^T = V \Sigma^T \Sigma V^T.\]
	Wobei es sich bei $\Sigma \Sigma^T \in \R^{n \times n}$ um eine Diagonalmatrix handelt auf deren Diagonalen sich die Eigenwerte von $M^T M$, also die Quadrate der Singulärwerte von $M$, befinden. Diese werden hier als $\sigma_{max}=\sigma_1,...,\sigma_n=\sigma_{min} \geq 0$ bezeichnet.
		
	Nun gilt für die 2-Norm von $M$:
	\begin{align*}
		\norm{M}_2^2
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \norm{Mx}_2^2                                                           \\
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \skprod{Mx}{Mx}                                                         \\
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \skprod{M^T Mx}{x}                                                      \\
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \skprod{V \Sigma^T \Sigma V^T x}{x}                                     \\
		  & \text{Aufgrunde der Orthogonalität von $V$ folgt}                                                        \\%!!!sieht blöd aus
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \skprod{\Sigma^T \Sigma x}{x}                                           \\
		= & \sup_{x \in \R^n,\norm{x} \leq 1} \sigma_1^2 x_1^2 + ... + \sigma_n^2 x_n^2 = \sigma_1^2 = \sigma_{max}^2 
	\end{align*}
	Der Beweis für die Behauptung über die lower Norm erfolgt genau analog, jedoch wird nicht das Supremum, sondern das Infimum gebildet und somit führt die Minimierung auf den kleinsten Singulärwert.
\end{proof}

Ein wesentlicher Unterschied zwischen den Operatoren auf unendlichen Vektorräumen und Matrizen auf endlichen ist, dass die Stetigkeit eines Operators nicht mehr garantiert ist. Doch auch hierbei kann die Norm helfen, denn es gilt.

\begin{theorem}
	Sei $T:X \to Y$ ein Operator, dann gilt
	\[ T \text{ \normalfont stetig } \iff \norm{T} < \infty.\]
\end{theorem}

\begin{proof}
	\
	\begin{enumerate}
		\item[$\Leftarrow$:] Sei $\norm{T} < \infty$, $x \in X$ und ${(x_n)}_{n \in \N}$ eine Folge, sodass $x_n \to x$, dann gilt 
		      \[\lim_{n \to \infty} \norm{Tx - Tx_n} \leq \norm {T} \cdot \lim_{n \to \infty} \norm{x - x_n}=0.\]
		\item[$\Rightarrow$:] Angenommen $T$ sei stetig und $\norm{T} = \infty$. Dies bedeutet, es existiert eine Folge $x_n$ in $X$, sodass
		      \[\norm{Tx_n} > n \norm{x_n}.\]
		      Dies ist eine direkte Folgerung aus der Unbeschränktheit des Supremums in der Definition der Norm. Nun wird $y_n \coloneqq \frac{x_n}{n \norm{x_n}}$ gesetzt (eine Division durch 0 kann hier nicht geschehen, da sonst die obige Ungleichung $0 < 0$ lautet würde). Jetzt ist $\norm{x_n -0} = \frac{1}{n}$, aber
		      \[\norm{T(y_n) - 0} = \frac{\norm{Tx_n}}{n \norm{x_n}} > 1.\]
		      In Worten, wir haben eine Nullfolge $y_n$ gefunden sodass $Ty_n$ nicht gegen Null konvergiert. Dies ist ein Widerspruch zur Stetigkeit.
	\end{enumerate}
\end{proof}

Weiterhin besteht ein im Folgenden wichtiger Zusammenhang zwischen der lower Norm eines Operators und der Norm der Inversen eines Operators, denn es gilt folgender Satz.
\begin{theorem}\label{thm:nuinv}
	Sei $T:X \to X$ ein bijektiver stetiger Operator auf dem Banachraum $X$ und $T^{-1}$ seine Inverse, dann gilt
	\[\norm{T^{-1}}=\frac{1}{\nu(T)}.\]
\end{theorem}

\begin{proof}
	\[\norm{T^{-1}} = \sup_{x \in X, x \neq 0} \frac{\norm{T^{-1}x}}{\norm{x}}\overset{\text{da $T$ bij.}}{=} \sup_{x \in Ty, y \in X, y \neq 0} \frac{\norm{T^{-1}x}}{\norm{x}} = \sup_{y \in X} \frac{\big\|\overbrace{T^{-1}Ty}^{y}\big\|}{\norm{Ty}}=\frac{1}{\inf_{y \in X}\frac{\norm{Ty}}{\norm{y}}}
	\]
	\[= \frac{1}{\inf_{y \in X}\frac{\norm{Ty}}{\norm{y}}} = \frac{1}{\nu(T)}\]
\end{proof}

In den folgenden Kapiteln sind jedoch nicht alle Operatoren gleich relevant, vorallem interessieren wir uns für Operatoren auf den $\ell^p(M)$ Räume. Speziell die Fälle $M=\N$ oder $M=\Z$. Diese Räume sind Folgenräume, also potentiell unendlich dimensionale Vektorräume, die hier über dem Körper der reellen Zahlen betrachtet werden.

\begin{definition}[$\ell^p$ Räume]
	Sei $M = \Z$ oder eine beliebige Teilmenge von $\Z$, etwa $\N$, dann ist 
	\[\ell^p(M) \coloneqq \Big\{ (x_n)_{n \in M} : x_n \in \R, \ n \in M, \ \sum_{n \in M} |x_n|^p <\infty  \Big\}\]
	ein Vektorraum. Für ein $x= (x_n)_{n \in M}$ lässt sich auf diesem Vektorraum eine Norm über 
	\[\norm{x}_p \coloneqq \Big(\sum_{n \in M} |x_n|^p \Big)^{\frac{1}{p}}\]
	definieren. Somit ergibt sich ein normierter Vektorraum.
\end{definition}

Speziell interessant ist jedoch der $\ell^2$. Zwar sind sämtliche $\ell^p$ Räume Banachräume, also vollständige metrische Vektorräume, jedoch ist nur der $\ell^2$ ein Hilbertraum, also ein Banachraum mit zusätzlichem Skalarprodukt.

\begin{definition}[$\ell^2$ Hilbertraum]
	Sei $\ell^2(M)$ der oben beschrieben Banachraum, dann bildet dieser zusammen mit dem Skalarprodukt
	\[\skprod{x}{y} \coloneqq \sum_{n \in M}x_n y_n \quad x,y \in \ell^2(M)\]
	einen Hilbertraum und es gilt
	\[\skprod{x}{x} = \norm{x}_2^2 \quad x \in \ell^2(M).\]
\end{definition}

Diese Räume spielen eine wichtige Rolle und im Folgenden werden häufig Operatoren zwischen ihnen auftreten.

\end{document}