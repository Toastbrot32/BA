function [ r ] = PrincipalPrefix( u )
n=size(u,2);
x=reverse(fibonacci(n-1));
if u == cat(2,x,0)
    r=x;
    return 
end
x=suffix(x,n-2);
if u == cat(2,x,[0,1])
    r=x;
    return
end
x=cat(2,x,[0,1]);
t=overlab(x,u);
m=size(t,2);
if m <= 1
    r=-1;
    return
else
    r=prefix(t,m-2);
end

end

