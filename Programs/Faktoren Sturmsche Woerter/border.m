function [ b ] = border( x )
m=size(x,2);
b=zeros(1,m+1);
i=0;
b(1)=-1;
for j=1:m-1
    b(j+1)=i;
    while (i >= 0) && (x(j+1) ~= x(i+1))
        i=b(i+1);
    end
    i=i+1;
end
b(m+1)=i;
end

