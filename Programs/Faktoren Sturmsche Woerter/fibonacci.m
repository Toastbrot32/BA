function [ F ] = fibonacci( n )
%FIBONACCI berechnet erst die kleinste Fib-Zahl mit n<fn
%Dann wird das standart Fib-Wort F mit der L�nge fn berechnet
%Ausgegeben werden die ersten n Zeichen von F

%Bestimme Fibonacci-Zahl, so dass n-1<=Fn
f1=1;
f2=1;
f=1;
index=1;
while f<n
    f=f1+f2;
    f2=f1;
    f1=f;
    index=index+1;
end

F1=0;
F2=1;
F=0;
for i=1:index
    F=[F1,F2];
    F2=F1;
    F1=F;
end

F=F(1:n);

end

