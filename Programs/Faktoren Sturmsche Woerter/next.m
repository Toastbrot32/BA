function [ w ] = next( u )
n=size(u,2);
r=PrincipalPrefix(u);
m=size(r,2);
if n == m+1
    w=cat(2,r,1);
    return
else
    s=suffix(u,n-m-2);
    w=cat(2,r,[1,0],s);
end

end

