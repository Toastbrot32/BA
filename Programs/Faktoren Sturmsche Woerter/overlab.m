function [ w ] = overlab( x,y )
m=size(y,2);
n=size(x,2);
b=border(y);
i=0; j=0;
while (i<m) && (j<n)
    while (i >= 0) && (y(i+1) ~= x(j+1))
        i=b(i+1);
    end
    i=i+1; j=j+1;
end

w=y(1:i);

end

