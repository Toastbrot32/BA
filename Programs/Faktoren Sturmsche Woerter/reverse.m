function [ x_reverse ] = reverse( x )
x_reverse = fliplr(x);
end

