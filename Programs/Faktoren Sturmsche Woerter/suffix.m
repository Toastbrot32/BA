function [ w ] = suffix( x, n )

m=size(x,2);
w=x(m-n+1:m);

end

