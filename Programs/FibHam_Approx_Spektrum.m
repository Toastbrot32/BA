%Approximiert das Spektrum des Fibonacci-Hamiltonian mit der in der BA beschriebenen Methode.

%eps = Maschienengenauigkeit
clear eps 

%F�ge Ordner mit den Funktionen zu Sturmschen W�rtern zum Pfad hinzu
addpath(pwd,'Faktoren Sturmsche Woerter');

%Anzahl der W�rter bzw. Gr��e der Matrizen
n=30;

%Fibonacci Hamiltonian - Generiere alle n+1 Untermatrizen
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;A(n+2,n)=1;
FibMatrizen=zeros(n+2,(n+1)*n);
for l=1:n+1
   A(2:n+1,:)=zeros(n)+diag(ones(n-1,1),-1)+diag(ones(n-1,1),1)+diag(Fib(l,:));
   FibMatrizen(:,1+(l-1)*n:l*n)=A;
end

%rechteckige Einheitsmatrix
I=zeros(n+2,n);
I(2:n+1,:)=eye(n);

%Feinheit des Gitters (gr��ter Abstand zwischen Gitterpunkten)
h=0.01;


%eps_n (Siehe Beweis von Approximationsmethode)
epsn=sym(2)*(pi/(n+1));

%Gitter - durchlaufe nur die reelle Achse
x=0:h:1; %Realteil
y=0; %Imagin�rteil = 0
mx=size(x,2);
my=size(y,2);
PS=zeros(mx,1);

FibMatrizen=sym(FibMatrizen);

for i=1:mx %Durchlaufe alle Gitterpunkte        
     %Entscheidung: ist x(i)+i*y(j) im Pseudospektrum?
     disp(x(i));
     index=1;
     sigminA=min(svd(FibMatrizen(:,1:n)-(x(i))*I));
     disp("--");
     %Breche ab, sobald x(i)+i*y(j) im PS von EINER der n+1 Matrizen
     %ist - berechne die anderen dann nicht mehr.
     while sigminA >= epsn + eps && index < n+1
         index = index +1;
         disp(index/(n+1));
         sigminA=min(svd((FibMatrizen(:,1+(index-1)*n:index*n))-(x(i))*I));
     end

     %In PS steht 1, falls x(i)+i*y(j) im PS von einer der n+1 Matrizen
     %ist, sonst 0
     if sigminA < epsn + eps
         PS(i,1)=1;
     end
     if PS(i,1)==1
         break;
     end
end
% 
% %Plot - hier: Barcode
% figure
% hold on
% set(gca, 'YTickLabel', '')
% set(gca,'Fontsize',14);
% %b=bar(x,PS);
% b=bar(x,PS,'FaceColor',[0 0 0],'EdgeColor',[0 0 0]); %Barcode
% b.BarWidth = 1;
% hold off

[mv,nv]=max(PS);

disp(1/(x(nv)-h))
disp(1/(x(nv)+h))