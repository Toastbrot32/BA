%Kleinster Singul�rwert der Untermatrizen

%eps = Maschienengenauigkeit
clear eps 

%F�ge Ordner mit den Funktionen zu Sturmschen W�rtern zum Pfad hinzu
addpath(pwd,'Faktoren Sturmsche Woerter');

%Anzahl der W�rter bzw. Gr��e der Matrizen
n=40;

%Fibonacci Hamiltonian - Generiere alle n+1 Untermatrizen
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;A(n+2,n)=1;
FibMatrizen=zeros(n+2,(n+1)*n);
for l=1:n+1
   A(2:n+1,:)=zeros(n)+diag(ones(n-1,1),-1)+diag(ones(n-1,1),1)+diag(Fib(l,:));
   FibMatrizen(:,1+(l-1)*n:l*n)=A;
end

%rechteckige Einheitsmatrix
I=zeros(n+2,n);
I(2:n+1,:)=eye(n);

%Feinheit des Gitters (gr��ter Abstand zwischen Gitterpunkten)
fein=0.01;

%eps_n (Siehe Beweis von Approximationsmethode)
epsn=2*(pi/(n+1));

%Gitter - durchlaufe nur die reelle Achse
x=-3:fein:3; %Realteil
y=0; %Imagin�rteil = 0
mx=size(x,2);
my=size(y,2);
sigmin=zeros(mx,my);
sig=zeros(n+1,1);


for i=1:mx
    for j=1:my
        
        %kleinsten Singul�rwert berechnen
        for l=1:n+1   
            sig(l,:)=min(svd(FibMatrizen(:,1+(l-1)*n:l*n)-(x(i)+1i*y(j))*I));
        end
        sigmin(i,j)=min(sig);
        
    end
end

figure
hold on
plot(x,sigmin);
plot(x,ones(mx,1)*(epsn+eps));
hold off