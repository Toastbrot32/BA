function [L,H] = Hess(A,m)
n=length(A);
L = zeros(n, m + 1);
H = zeros(m + 1, m);
L(1 : n, 1) = [1     0     1     1     0     0     0     0     0     0];
for k = 1 : m
    r = A*L(1 : n, k);
    H(1 : k, k) = inv(L(1 : k, 1 : k))*r(1 : k);
    L(k + 1 : n, k + 1) = r(k + 1 : n) - L(k + 1 : n, 1 : k)*H(1 : k, k);
    H(k + 1, k)= 1;
end

end

