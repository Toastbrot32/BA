k=200;
tic;
for n=164
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
FibMatrizen=zeros(n,n,n+1);
for l=1:n+1
   %A(2:n+1,:)=zeros(n)+diag(ones(n-1,1),-1)+diag(ones(n-1,1),1)+diag(Fib(l,:));
   A=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
   FibMatrizen(:,:,l)=A;
end
    svmat=[];
    n
for i=1:n+1
    %eigsmallest(i)=eigs(FibMatrizen(:,:,i),1,'smallestabs');
    svmat(i)=min((svd(sym(FibMatrizen(:,:,i)))));
end
[mv,nv]=min(svmat);
 mineig(n)=mv;
 minstelle(n)=nv;
 [V,D,W] = eig(FibMatrizen(:,:,nv));
 [m,v]=min(abs(diag(D)));
 Vmin(1:n,n)=V(:,v);
 last(n)=abs(V(n,v));
%diag(FibMatrizen(:,:,nv))
%[diag(FibMatrizen(:,:,nv)),[1,fib(n-1)]']
%loglog(1:k,last);
%plot(1:k,mineig);
end
toc