function [ A ] = sturm2( n )
A=zeros(n+1,n);
x=fibonacci(n-1);
A(1,:)=cat(2,0,x);
A(n+1,:)=cat(2,1,x);
index=1;
while ~(isequal(A(index,:),A(n+1,:)))
    A(index+1,:)=next(A(index,:));
    index=index+1;
end



