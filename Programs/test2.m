n=50;
for n=1:10
Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
FibMatrizen=zeros(n,n,n+1);
for l=1:n+1
   %A(2:n+1,:)=zeros(n)+diag(ones(n-1,1),-1)+diag(ones(n-1,1),1)+diag(Fib(l,:));
   A=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
   FibMatrizen(:,:,l)=A;
end

svmat=[];

for i=1:n+1
    svmat(i)=min(sym(svd(FibMatrizen(:,:,i))));
end

[mv,nv]=min(svmat);
diag(FibMatrizen(:,:,nv));
[diag(FibMatrizen(:,:,nv)),[1,1.-fibonacci(n-1)]'];

[nv,n];

minew(n)=mv;

p(n)=det(FibMatrizen(:,:,1)-eye(n)*sym(x))

end