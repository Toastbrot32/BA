n=2000;

% Fib=sturm(n);
% Fib=ones(n+1,n)-Fib;
% A=zeros(n+2,n);
% A(1,1)=1;
% A(n+2,n)=1;
% FibMatrizen=zeros(n+2,n,n+1);
% for l=1:n+1
%    A(2:n+1,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
%    FibMatrizen(:,:,l)=A;
% end

Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+1,n);
%A(1,1)=1;
A(n+1,n)=1;
FibMatrizen=zeros(n+1,n,n+1);
for l=1:n+1
   A(1:n,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
   FibMatrizen(:,:,l)=A;
end

disp('---')

sv=zeros(1,n+1);
for j=1:n+1;
    sv(j)=min(svd(FibMatrizen(:,:,j)));
    disp(j/(n+1))
end

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));

[1/(val),1/(val-delta)]

%3.510810540273527   7.709871025098552