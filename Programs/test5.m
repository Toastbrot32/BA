n=10;

sv=zeros(1,n+1);

Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+1,n);
%A(1,1)=1;
A(n+2,n)=1;
sv=zeros(1,n+1);

%FibMatrizen=zeros(n+1,n,n+1);
for l=1:n+1
    A(2:n+1,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
    sv(l)=min(svds(A,20,'smallest'));
    disp(l/(n+1))
end

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));

[1/(val),1/(val-delta)]

%3.510810540273527   7.709871025098552 sym n= 85 zwei
%7.700916861289012   8.092276352881688 num n=2000 ein
%4.941548135294545   5.019432518552129 num n=2000 zwei 