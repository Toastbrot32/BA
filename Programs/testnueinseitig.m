addpath(pwd,'Faktoren Sturmsche Woerter');

fileID = fopen('datanueinseitig.txt','w');

fprintf(fileID,'xcol ycol error \n');


for n=10*[10:1000]

Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+1,n);
%A(1,1)=1;
A(n+1,n)=1;
sv=zeros(1,n+1);

%FibMatrizen=zeros(n+1,n,n+1);
for l=1:n+1
    A(1:n,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
    sv(l)=min(svds(A,100,'smallest'));
end

disp(n/(1000))

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));

iv=[1/(val),1/(val-delta)];

fprintf(fileID,'%d %f %f \n',n,(iv(2)+iv(1))/2,abs(iv(2)-iv(1)));

end
