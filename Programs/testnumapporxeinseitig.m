k=10000;
v=fibham(20);
M2=zeros(1,k);

fileID2 = fopen('data1.txt','a');

%fprintf(fileID2,'xcol ycol \n');

fileID1 = fopen('data1_1.txt','a');

%fprintf(fileID1,'xcol ycol wganz w100 \n');

for n=4054:k
  A=(diag(ones(1,n-1),1) + diag(v(1:n)) + diag(ones(1,n-1),-1));
  %A=sym(diag(ones(1,n-1),1) + diag(v(1:n)) + diag(ones(1,n-1),-1));
  %M2(n)=1/min(svd(A));
  M2(n)=1/min(svd(A));
  disp(n/k)
  if M2(n)>7.5    
    a=mat2str(v(1:n));
    
    a(a=='[' )='';
    a(a==']' )='';
    a(a==' ' )='';
    
    if n>101
    	b=mat2str(v(n-100:n));
    else
        b=mat2str(v(1:n));
    end
    
    
    b(b=='[' )='';
    b(b==']' )='';
    b(b==' ' )='';
    
    fprintf(fileID1,'%d %.16f %s %s \n',n,M2(n),a,b);
  end
  fprintf(fileID2,'%d %.16f \n',n,M2(n));
end

