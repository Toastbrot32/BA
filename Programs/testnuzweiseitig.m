addpath(pwd,'Faktoren Sturmsche Woerter');

fileID = fopen('datanuzweiseitig2.txt','w');

fprintf(fileID,'xcol ycol error \n');

fprintf(fileID1,'xcol ycol error minsvd ivleft ivright \n');

for n=200

Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
sv=zeros(1,n+1);

for l=1:n+1
    A(2:n+1,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
    sv(l)=min(svd(A));
end

A=zeros(n+1,n);
A(n+1,n)=1;

for l=1:n+1
    A(1:n,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
    sv(l+n+1)=min(svd(A));
end

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n));

iv=[1/(val),1/(val-delta)]

fprintf(fileID1,'%d %.16f %.16f %.16f %.16f %.16f \n',n,(iv(2)+iv(1))/2,abs(iv(2)-iv(1)),val,iv(1),iv(2));

end

fclose(fileID);
