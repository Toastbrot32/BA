addpath(pwd,'Faktoren Sturmsche Woerter');

fileID1 = fopen('datazwnu.txt','w');
fileID2 = fopen('dataeinnu.txt','w');

fprintf(fileID1,'xcol ycol error minsvd ivleft ivright \n');
fprintf(fileID2,'xcol ycol error minsvd ivleft ivright \n');

v=fibham(20);

vec=[2000];

for n=vec

Fib=sturm(n);
Fib=ones(n+1,n)-Fib;
A=zeros(n+2,n);
A(1,1)=1;
A(n+2,n)=1;
sv=zeros(1,n+1);

for l=1:n+1
    A(2:n+1,:)=diag(ones(1,n-1),1) + diag(Fib(l,:)) + diag(ones(1,n-1),-1);
    sv(l)=min(svd(A));
    disp(l)
    fflush(stdout());
end

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));

iv=[1/(val),1/(val-delta)];

fprintf(fileID1,'%d %.16f %.16f %.16f %.16f %.16f \n',n,(iv(2)+iv(1))/2,abs(iv(2)-iv(1)),val,iv(1),iv(2));

A=zeros(n+1,n);
A(1:n,:)=diag(ones(1,n-1),1) + diag(v(1:n)) + diag(ones(1,n-1),-1);
A(n+1,n)=1;
sv(n+2)=min(svd(A));

[val,nv]=min(sv);
delta = 4*sin(pi/(2*n+2));

iv=[1/(val),1/(val-delta)];

fprintf(fileID2,'%d %.16f %.16f %.16f %.16f %.16f \n',n,(iv(2)+iv(1))/2,abs(iv(2)-iv(1)),val,iv(1),iv(2));

disp(n);
fflush(stdout());

end

fclose(fileID1);
fclose(fileID2);
